<?php

namespace App\Classes;

use App\Ruta;

class CotizacionService
{

    /**
     * Function to calculate the trip and the value
     *
     * @return $result The final calculation
     */
    public function cotizar(string $origen, string $destino)
    {
        $result = [];
        $result['isOk'] = false;
        $result['trip'] = [];

        // Validate if there is a direct bus
        $ruta = Ruta::where(['origen_ciudad' => $origen, 'destino_ciudad' => $destino])->first();

        if (!empty($ruta)) {
            $result['isOk'] = true;
            $result['trip'][] = $ruta;

        } else {

            // Get the busses that starts from the selected origin
            $origenes = Ruta::where(['origen_ciudad' => $origen])->get();
                if (!empty($origenes)) {

                    foreach ($origenes as $ori) {

                        if (!$result['isOk']) {

                            $trip = [];
                            $trip[] = $ori;

                            // Calculate the route for each bus that starts from the origin
                            $result = $this->calcularRuta(
                                $result,
                                $ori,
                                $destino,
                                0,
                                $trip
                            );
                        } else {
                            break;
                        }
                    }
                }
        }


        return $result;
    }
    /**
     * Recursive function to calculate the route. Added a cont var to prevent an infinite loop
     *
     * @param  $result Value to be passed each iteration of the function to be returned later 
     * @param  $ori Current origin Ruta
     * @param  $destino Final destination
     * @param  $cont Value to prevent an infinite loop
     * @param  $trip The current trip
     * @return $result The result of calculation
     */
    public function calcularRuta(array $result, $ori, string $destino, int $cont, array $trip) {
        
        $cont++;

        $second = Ruta::where(['origen_ciudad' => $ori->destino_ciudad])->get();
        foreach ($second as $sec) {

            if ($sec->destino_ciudad == $destino) {
                $result['isOk'] = true;
                $result['trip'] = $trip;
                $result['trip'][] = $sec;
                break;
            } else if ($cont<4) {
                // Keep calculating the route until we reach our destination
                $trip[] = $sec;
                $result = $this->calcularRuta($result, $sec, $destino, $cont, $trip);
            } else if ($cont>=4) {
                break;
            }

            // Reset the trip when no complete route was found
            $trip = [];
            $trip[] = $ori;
        }

        return $result;
    }


}
