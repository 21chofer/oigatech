<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model
{
    protected $table = 'ruta';
    protected $fillable = ['nombre_ruta', 'origen_ciudad', 'destino_ciudad', 'bus_Id', 'valor_pasaje'];

    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }
}
