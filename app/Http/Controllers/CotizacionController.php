<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ruta;
use App\Classes\CotizacionService;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $origenes = ruta::distinct()->select('origen_ciudad')->orderBy('origen_ciudad', 'ASC')->get();
        $destinos = ruta::distinct()->select('destino_ciudad')->orderBy('destino_ciudad', 'ASC')->get();

        return view('cotizacion.index',
            [
                "origenes" => $origenes, 
                "destinos" => $destinos
            ]
        );
    }

    public function inicioCotizar (Request $request) {

        $service = new CotizacionService();
        $result = $service->cotizar($request->origen, $request->destino);

        $response = [
            'result' => $result
        ];

        return response()->json($response, 200);
    }

}
