<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $table = 'bus';
    protected $fillable = ['modelo', 'nombre_conductor'];

    public function ruta()
    {
        return $this->hasMany('App\Ruta');
    }
}
