<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">

    <title>MY BUS LLC</title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

    <h1>MY BUS LLC</h1>

	<div class="container-fluid" style="margin-top: 50px">

		@yield('content')
	</div>

<style type="text/css">
.table {
    border-top: 2px solid #ccc;
}
.w-sm {
    width: 100px;
}
*{
    font-family: 'Segoe UI',Frutiger,'Frutiger Linotype','Dejavu Sans','Helvetica Neue',Arial,sans-serif;
    font-size: 1.03em;
}
.alert, .btn {
    font-size: 1.02em !important;
}



</style>
</body>
</html>