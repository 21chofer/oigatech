@extends('layouts.app')
@section('content')
<div class="col-md-5 col-md-offset-1">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo bus</h3>
        </div>
        <div class="table-container">
            <form method="POST" action="{{ route('bus.store') }}"  role="form">
                {{ csrf_field() }}
                <table class="table">
                    <tr>
                        <th>Modelo</th>
                        <td>
                            <input type="number" name="modelo" id="modelo" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <th>Nombre Conductor</th>
                        <td>
                            <input type="text" name="nombre_conductor" id="nombre_conductor" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-success btn-block">
                                Guardar
                            </button>
                        </td>
                    </tr>

                </table>
            </form>
        </div>

    </div>
    <a href="{{ route('bus.index') }}" class="btn btn-info btn-block w-sm" >Atrás</a>
</div>

@endsection