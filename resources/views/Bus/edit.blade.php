@extends('layouts.app')
@section('content')

<div class="col-md-8 col-md-offset-2">

    @if(Session::has('success'))
    <div class="alert alert-info">
        {{Session::get('success')}}
    </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Actualizar bus id = {{ $bus->id }}</h3>
        </div>
        <div class="table-container">
            <form method="POST" action="{{ route('bus.update',$bus->id) }}"  role="form">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">

                <table class="table">
                    <tr>
                        <th>Nombre</th>
                        <td>
                            <input type="number" name="modelo" id="modelo" class="form-control input-sm" value="{{ $bus->modelo }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>
                            <input type="text" name="nombre_conductor" id="nombre_conductor" class="form-control input-sm"  value="{{ $bus->nombre_conductor }}"  >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary btn-block">
                                Actualizar
                            </button>
                        </td>
                    </tr>

                </table>
            </form>
        </div>
    </div>
    <a href="{{ route('bus.index') }}" class="btn btn-info btn-block w-sm" >Atrás</a>
</div>

@endsection