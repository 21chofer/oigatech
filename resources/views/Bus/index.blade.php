@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-1">

    <div class="pull-left"><h3>Lista buses</h3></div>
    <div class="pull-right">

        <a href="{{ route('bus.create') }}"> 
            <button class="btn btn-info" >Añadir bus</button>
        </a>

    </div>

    <div class="table-container">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Id</th>
                <th>Modelo</th>
                <th>Conductor</th>
                <th colspan="2">Acciones</th>
            </thead>
            <tbody>
                @if($bus->count())  
                    @foreach($bus as $item)  
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->modelo}}</td>
                        <td>{{$item->nombre_conductor}}</td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="{{action('BusController@edit', $item->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a>
                        </td>
                        <td>
                            <form action="{{action('BusController@destroy', $item->id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">

                                <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach 
                @else
                    <tr>
                        <td colspan="4">No hay registros !!</td>
                    </tr>
                @endif
            </tbody>
        </table>
       
    </div>
</div>


@endsection