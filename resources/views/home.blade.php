@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    <div class="col-md-4">
                        <a href="/cotizacion" target="_blank">
                            <button class="btn btn-primary">
                                <span class="glyphicon glyphicon-usd"></span>
                                Cotizar ruta
                            </button>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/bus">
                            <button class="btn btn-primary">
                                <span class="glyphicon glyphicon-plane"></span>
                                Agregar Buses
                            </button>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="/ruta">
                            <button class="btn btn-primary">
                                <span class="glyphicon glyphicon-road"></span>
                                Agregar Rutas
                            </button>
                        </a>
                    </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
