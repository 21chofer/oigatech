@extends('layouts.app')
@section('content')

<div class="col-md-5 col-md-offset-1">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Nueva Ruta</h3>
        </div>
        <div class="table-container">

            <form method="POST" action="{{ route('ruta.store') }}"  role="form">
                {{ csrf_field() }}
                <table class="table">

                    <tr>
                        <th>Nombre Ruta</th>
                        <td>
                            <input type="text" name="nombre_ruta" id="nombre_ruta" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <th>Origen</th>
                        <td>
                            <input type="text" name="origen_ciudad" id="origen_ciudad" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <th>Destino</th>
                        <td>
                            <input type="text" name="destino_ciudad" id="destino_ciudad" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <th>Bus</th>
                        <td>
                            <select name="bus_Id">
                                @foreach($buses as $item) 
                                    <option value="{{ $item->id }}">{{ $item->modelo . " - " . $item->nombre_conductor}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Pasaje</th>
                        <td>
                            <input type="number" name="valor_pasaje" id="valor_pasaje" class="form-control input-sm" >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-success btn-block">
                                Guardar
                            </button>
                        </td>
                    </tr>

                </table>
            </form>
        </div>

    </div>
    <a href="{{ route('ruta.index') }}" class="btn btn-info btn-block w-sm" >Atrás</a>
</div>

@endsection