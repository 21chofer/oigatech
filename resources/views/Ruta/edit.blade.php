@extends('layouts.app')
@section('content')

<div class="col-md-8 col-md-offset-2">

    @if(Session::has('success'))
    <div class="alert alert-info">
        {{Session::get('success')}}
    </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Actualizar ruta id = {{ $ruta->id }}</h3>
        </div>
        <div class="table-container">
            <form method="POST" action="{{ route('ruta.update',$ruta->id) }}"  role="form">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">

                <table class="table">
                    <tr>
                        <th>Nombre ruta</th>
                        <td>
                            <input type="text" name="nombre_ruta" id="nombre_ruta" class="form-control input-sm" value="{{ $ruta->nombre_ruta }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Origen</th>
                        <td>
                            <input type="text" name="origen_ciudad" id="origen_ciudad" class="form-control input-sm" value="{{ $ruta->origen_ciudad }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Destino</th>
                        <td>
                            <input type="text" name="destino_ciudad" id="destino_ciudad" class="form-control input-sm" value="{{ $ruta->destino_ciudad }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Bus_Id</th>
                        <td>
                            <input type="text" name="bus_Id" id="bus_Id" class="form-control input-sm" value="{{ $ruta->bus_Id }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Pasaje</th>
                        <td>
                            <input type="text" name="valor_pasaje" id="valor_pasaje" class="form-control input-sm"  value="{{ $ruta->valor_pasaje }}"  >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-primary btn-block">
                                Actualizar
                            </button>
                        </td>
                    </tr>

                </table>
            </form>
        </div>
    </div>
    <a href="{{ route('ruta.index') }}" class="btn btn-info btn-block w-sm" >Atrás</a>
</div>

@endsection