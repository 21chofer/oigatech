@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-1">

    <div class="pull-left"><h3>Lista rutas</h3></div>
    <div class="pull-right">

        <a href="{{ route('ruta.create') }}"> 
            <button class="btn btn-info" >Añadir ruta</button>
        </a>

    </div>

    <div class="table-container">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Id</th>
                <th>Ruta</th>
                <th>Origen</th>
                <th>Destino</th>
                <th>Bus</th>
                <th>Pasaje</th>

                <th colspan="2">Acciones</th>
            </thead>
            <tbody>
                @if($ruta->count())  
                    @foreach($ruta as $item)  
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->nombre_ruta}}</td>
                        <td>{{$item->origen_ciudad}}</td>
                        <td>{{$item->destino_ciudad}}</td>
                        <td>{{$item->bus_Id}}</td>
                        <td>{{$item->valor_pasaje}}</td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="{{action('RutaController@edit', $item->id)}}" ><span class="glyphicon glyphicon-pencil"></span></a>
                        </td>
                        <td>
                            <form action="{{action('RutaController@destroy', $item->id)}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">

                                <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach 
                @else
                    <tr>
                        <td colspan="4">No hay registros !!</td>
                    </tr>
                @endif
            </tbody>
        </table>
       
    </div>
</div>


@endsection