@extends('layouts.layout')
@section('content')


<div class="col-md-8 col-md-offset-1">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Cotizacion de rutas</h3>
        </div>
        <div class="table-container">
            <div class="row"></div>

            <div class="row">
                <div class="col-sm-3">
                    <b>Origen</b>
                    <select id="origen">
                    @foreach($origenes as $item)
                        <option>{{ $item->origen_ciudad }}</option>
                    @endforeach
                    </select>

                </div>
                <div class="col-sm-4">
                    <b>Destino</b>
                    <select id="destino">
                    @foreach($destinos as $item)
                        <option>{{ $item->destino_ciudad }}</option>
                    @endforeach
                    </select>

                </div>
                <div class="col-sm-2">

                    <button id="calcular" class="btn btn-success">
                        <span class="glyphicon glyphicon-search"></span>
                        Calcular
                    </button>

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <div id="msg"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                        <span class="glyphicon glyphicon-plane"></span>
                        <div id="tripTexto" style="display: inline"></div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                        <span class="glyphicon glyphicon-usd"></span>
                        <div id="valorTotal" style="display: inline"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                        <span class="glyphicon glyphicon-plane"></span>
                        <div id="tripDetallado" style="display: inline"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
$(document).ready(function(){

    $("#calcular").click(function() {

        var origen = $("#origen").val();
        var destino = $("#destino").val();

        if (origen == destino) {
            $("#msg").html("<div class='alert alert-warning'>Ciudad de origen es igual a la Ciudad destino</div>");
        } else {

            $.ajax({
                type: "GET",
                dataType: 'json',
                url: "/inicioCotizar",
                data: {
                    'origen': origen,
                    'destino': destino,
                },
                success: function (data) {

                    var msg = "No se encontro esta ruta";
                    var msgClass = "warning";
                    var valor = "";
                    var tripTexto = "";
                    var tripDetallado = "";

                    data = data.result;

                    if (data.isOk) {

                        valor = 0;

                        // Add origen city to the trip text
                        tripTexto = data.trip[0].origen_ciudad;

                        $.each(data.trip, function(index, current) {
                            valor = parseInt(valor) + parseInt(current.valor_pasaje);

                            // Create the short text for the trip
                            tripTexto += " -> " + current.destino_ciudad;

                            // Create a detail text for the trip
                            tripDetallado += "Desde " + current.origen_ciudad + " hasta " + current.destino_ciudad + " en Bus " + current.bus_Id + " con valor de $" + current.valor_pasaje + ". ";

                        });

                        msgClass = "info";
                        msg = "Ruta encontrada";

                        valor = "Valor pasaje $" + valor;
                    }

                    $("#msg").html("<div class='alert alert-" + msgClass + "' >" +  msg + "</div>");
                    $("#tripTexto").text(tripTexto);
                    $("#valorTotal").text(valor);
                    $("#tripDetallado").text(tripDetallado);
                }
            });
        }
    });

});
</script>

<style>

.row{
    min-height: 50px;
    padding: 2px;
    vertical-align: middle;
}

</style>


@endsection