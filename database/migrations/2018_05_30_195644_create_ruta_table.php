<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_ruta');
            $table->string('origen_ciudad');
            $table->string('destino_ciudad');
            $table->integer('bus_Id');
            $table->integer('valor_pasaje');
            $table->timestamps();

            $table->foreign('bus_Id')
                ->references('id')
                ->on('bus')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruta');
    }
}
